![Screenshot preview of the theme "Drizzle" by glenthemes](https://64.media.tumblr.com/0e3aa2fb82025e7f7d0716fb87282641/tumblr_pq1p7uFoIq1ubolzro2_1280.png)

**Theme no.:** 34  
**Theme name:** Drizzle  
**Theme type:** Free / Tumblr use  
**Description:** A matching theme for my [Breeze](https://glenthemes.tumblr.com/post/184152595479) about page, suggested by [anonymous](https://glenthemes.tumblr.com/post/184152791369/the-sidebar-thingy-in-your-breeze-page-theme-would).  
**Author:** @&hairsp;glenthemes  

**Release date:** 2019-04-16  
**Last updated:** 2022-10-22

**Post:** [glenthemes.tumblr.com/post/184222280229](https://glenthemes.tumblr.com/post/184222280229)  
**Preview:** [glenthpvs.tumblr.com/drizzle](https://glenthpvs.tumblr.com/drizzle)  
**Download:** [pastebin.com/x8TfSPwg](https://pastebin.com/x8TfSPwg)  
**Credits:** [glenthemes.tumblr.com/drizzle-crd](https://glenthemes.tumblr.com/drizzle-crd)
